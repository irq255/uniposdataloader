SET unique_checks = 0;
SET foreign_key_checks = 0;
SET sql_log_bin = 0;
SET autocommit = 0;
set global innodb_io_capacity = 400;
set global innodb_io_capacity_max = 10000;
#set innodb_autoinc_lock_mode = 0;

ALTER TABLE event DISABLE KEYS;
ALTER TABLE transaction DISABLE KEYS;

	
	
#show variables like 'innodb_%'

/*
SET unique_checks = 0;
SET foreign_key_checks = 0;
SET sql_log_bin = 0;
SET autocommit = 0;
ALTER TABLE event DISABLE KEYS;
ALTER TABLE transaction DISABLE KEYS;
set global innodb_io_capacity = 200;
set global innodb_io_capacity_max = 2000;
*/

