drop procedure if exists generateVirtualTerminals;

DELIMITER //
CREATE PROCEDURE `generateVirtualTerminals`()
begin

declare virtualTerminalId int default 0;
declare counter int default 0;

select max(id) into virtualTerminalId from virtual_terminal;

while counter < 40000
do
set virtualTerminalId = virtualTerminalId + 1;
set counter = counter + 1;
insert into `virtual_terminal` (id
					,virtualId
					,bank_id
					,terminal_id
					,active
					,state_id
					,blockDate
					,blockCause
					,lastTransactionId)
						
					values(virtualTerminalId
					,concat("PTEST", counter)
					,NULL
					,5
					,0
					,4
					,"2016-08-29 12:32:19"
					,1
					,36107);
end while;
end//
DELIMITER ;

call generateVirtualTerminals();