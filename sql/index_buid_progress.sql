SELECT sql.sql_text
		, sess.process
		, sess.status
		, sess.username
		, sess.schemaname
		, sql.USER_IO_WAIT_TIME
		, sql.PLSQL_EXEC_TIME
		, sql.CPU_TIME
		, sql.ELAPSED_TIME
		, sql.LAST_ACTIVE_TIME
		, sql.PHYSICAL_WRITE_BYTES
		FROM v$session sess,
       v$sql     sql
 WHERE sql.sql_id(+) = sess.sql_id
   AND sess.schemaname = 'SYS'
   AND SQL.sql_text IS NOT null