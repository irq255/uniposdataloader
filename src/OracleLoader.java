
import java.io.*;
import java.util.Scanner;

public class OracleLoader implements Runnable {

    Config config = new Config();
    String url = config.getProperty("database");
    String SID = config.getProperty("SID");
    String username = config.getProperty("user");
    String userpassword = config.getProperty("password");

    String script = null;
    File scriptFile = null;


    public OracleLoader(String script) {
        this.script = script;
        scriptFile = new File(script);

    }

    @Override
    public void run() {
        load();

    }

    String readScriptFile(File filepath)  {

        StringBuilder fileContent = new StringBuilder();
        try {
            Scanner scanner = new Scanner(filepath);



            while (scanner.hasNext()) {
                fileContent.append(scanner.nextLine() + "\n");
            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return fileContent.toString();

    }

    void load() {

        String shellScript = script;
        try {
            Process process = new ProcessBuilder(shellScript).start();

            BufferedReader scriptOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            BufferedReader scriptError = new BufferedReader(new InputStreamReader(process.getErrorStream()));


            // Get script output or errors

            String scriptOutputLine = null;
            while ((scriptOutputLine = scriptOutput.readLine()) != null) {
                System.out.println(scriptOutputLine);
            }
            String scriptErrorLine = null;
            while ((scriptErrorLine = scriptError.readLine()) != null) {
                System.out.println(scriptErrorLine);
            }
            scriptError.close();
            scriptOutput.close();

            process.waitFor();


        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {

        }
    }



}
