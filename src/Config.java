import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class Config {

    Properties props = new Properties();

    public String getProperty(String key) {
        try

        {
            props.load(new FileInputStream("config.properties"));
        } catch(IOException ex){
            ex.printStackTrace();
        }
        return props.getProperty(key);
    }


}
