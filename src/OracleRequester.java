import java.sql.*;

public class OracleRequester {

    Config config = new Config();
    String url = config.getProperty("database");
    String username = config.getProperty("user");
    String userpassword = config.getProperty("password");

    Connection connection = null;
    PreparedStatement preparedStatement = null;



    public void setConnection() {


        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");

            connection = DriverManager.getConnection(url, username, userpassword);


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public long getLastTransactionId() {
        long id = 0;

        try {
            setConnection();
            preparedStatement = connection.prepareStatement("SELECT max(id) id FROM TRANSACTION");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            id = resultSet.getLong("id");
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("Start generate transactions from id: " + id);
        return id;

    }

    public long getLastIndexTestRebuildId() {
        long id = 0;

        try {
            setConnection();
            preparedStatement = connection.prepareStatement("SELECT max(id) id FROM TRANSACTION");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            id = resultSet.getLong("id");
            connection.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("Start generate transactions from id: " + id);
        return id;

    }

    public long getLastEventId() {
        long id = 0;

        try {
            setConnection();
            preparedStatement = connection.prepareStatement("SELECT max(id) id FROM EVENT");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            id = resultSet.getLong("id");
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println("Start generate events from id: " + id);
        return id;

    }

    public int getIdOfFirstVirtualTerminal() {
        int id = 0;

        try {
            setConnection();
            preparedStatement = connection.prepareStatement("select id From virtual_terminal where virtualid = '66600001'");
            ResultSet resultSet = preparedStatement.executeQuery();
            resultSet.next();
            id = resultSet.getInt("id");
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Can't get id of first virtual terminal. Probably you didn't create them. " +
                    "Try to run generateVirtualTerminals.sql\nOr check db connection url. Now: " + url + "\n");



        }
        System.out.println("ID of first virtual terminal : " + id);
        return id;

    }

}

