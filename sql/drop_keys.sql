# remove transaction index

ALTER TABLE transaction DROP FOREIGN KEY fk_tran_card_input;
ALTER TABLE transaction DROP FOREIGN KEY fk_tran_cur;
ALTER TABLE transaction DROP FOREIGN KEY fk_tran_event;
ALTER TABLE transaction DROP FOREIGN KEY fk_tran_pay_system;
ALTER TABLE transaction DROP FOREIGN KEY fk_tran_response;
ALTER TABLE transaction DROP FOREIGN KEY fk_tran_virt_term;
ALTER TABLE transaction DROP INDEX FKTransactio181655;
ALTER TABLE transaction DROP INDEX FKTransactio392658;
ALTER TABLE transaction DROP INDEX FKTransactio403039;
ALTER TABLE transaction DROP INDEX FKTransactio546647;
ALTER TABLE transaction DROP INDEX FKTransactio57240;
ALTER TABLE transaction DROP INDEX FKTransactio79715;


# remove event index

ALTER TABLE event DROP FOREIGN KEY fk_event_host_resp;
ALTER TABLE event DROP FOREIGN KEY fk_event_inp_event;
ALTER TABLE event DROP FOREIGN KEY fk_event_term;
ALTER TABLE event DROP INDEX FK_event_inp_host_response_id;
ALTER TABLE event DROP INDEX FKevent291258;
ALTER TABLE event DROP INDEX FKevent429659;