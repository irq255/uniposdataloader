package test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestProcessBuilder implements Runnable{


    public void run2() {
        String[] script = {"echo", Thread.currentThread().getName()};
        try {
            Process process = new ProcessBuilder(script).start();
            process.waitFor();

            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String processOutput;
            while ((processOutput = reader.readLine()) != null) {
                System.out.println(processOutput);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void run() {
        System.out.println("hello");

    }
}
