import java.io.BufferedWriter;
import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;

class UniPosEvent {


    int value = 0;

    int[] hostResponseId = {1, 2, 3, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
            , 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39
            , 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59
            , 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79
            , 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99
            , 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 112, 113, 114, 120, 131, 132
            , 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148
            , 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164
            , 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180
            , 181, 182, 183, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 370, 371
            , 372, 373, 374, 375, 376, 377, 378, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007
            , 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021
            , 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035
            , 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049
            , 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058};
    String responseCode = "04";

    int[] eventId  = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
            , 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40
            , 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60
            , 61, 62, 63, 64, 65, 66, 67, 72, 73, 74, 75, 76, 77, 78};

    long eventPrimaryid = 0;

    UniPosEvent() {

    }


    void generateEvents(BufferedWriter eventsFile, int amountOfEventsPerTransaction, long datetime, int terminalId, long lastEventId) throws IOException {

        StringBuilder oneEvent = new StringBuilder();
        String separator = ",";
        eventPrimaryid = lastEventId+1;

        for (int i = 0; i < amountOfEventsPerTransaction; i++) {
            oneEvent.delete(0, oneEvent.length());

            oneEvent.append(eventPrimaryid)
                    .append(separator)
                    .append(datetime)
                    .append(separator)
                    .append(getEventID())
                    .append(separator)
                    .append(terminalId)
                    .append(separator)
                    .append(value)
                    .append(separator)
                    .append(getHostResponseId())
                    .append(separator)
                    .append(responseCode)
                    .append("\n");

            eventsFile.write(oneEvent.toString());
            eventPrimaryid++;

        }
    }

    int getEventID() {
        return eventId[ThreadLocalRandom.current().nextInt(eventId.length)];
    }

    int getHostResponseId() {
        return hostResponseId[ThreadLocalRandom.current().nextInt(hostResponseId.length)];
    }
}