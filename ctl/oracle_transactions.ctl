load data
infile '/opt/test/oraloader/data/transactions.csv' into table transaction
append
FIELDS TERMINATED BY ','
	(id
		,date_time
		,runtime
		,try_count
		,payment_system_id
		,card_input_type_id
		,response_id
		,event_id
		,virtual_terminal_id
		,currency_id
		,bin
		,response_code
		,amount_decimal
		,creation_date DATE 'YYYY-MM-DD HH24:MI:SS'
		,transaction_date DATE 'YYYY-MM-DD HH24:MI:SS')
