
# create index and constraints on transaction

ALTER TABLE transaction ADD KEY `FKTransactio181655` (`currency_id`);
ALTER TABLE transaction ADD KEY `FKTransactio392658` (`response_id`);
ALTER TABLE transaction ADD KEY `FKTransactio403039` (`virtual_terminal_id`);
ALTER TABLE transaction ADD KEY `FKTransactio546647` (`event_id`);
ALTER TABLE transaction ADD KEY `FKTransactio57240` (`payment_system_id`);
ALTER TABLE transaction ADD KEY `FKTransactio79715` (`card_input_type_id`);
ALTER TABLE transaction ADD CONSTRAINT `fk_tran_card_input` FOREIGN KEY (`card_input_type_id`) REFERENCES `inp_card_input_type` (`id`);
ALTER TABLE transaction ADD CONSTRAINT `fk_tran_cur` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`);
ALTER TABLE transaction ADD CONSTRAINT `fk_tran_event` FOREIGN KEY (`event_id`) REFERENCES `inp_event` (`id`);
ALTER TABLE transaction ADD CONSTRAINT `fk_tran_pay_system` FOREIGN KEY (`payment_system_id`) REFERENCES `inp_payment_systems` (`id`);
ALTER TABLE transaction ADD CONSTRAINT `fk_tran_response` FOREIGN KEY (`response_id`) REFERENCES `inp_host_response` (`id`);
ALTER TABLE transaction ADD CONSTRAINT `fk_tran_virt_term` FOREIGN KEY (`virtual_terminal_id`) REFERENCES `virtual_terminal` (`id`);


# create index and constraints on event

ALTER TABLE event ADD KEY `FK_event_inp_host_response_id` (`host_response_id`);
ALTER TABLE event ADD KEY `FKevent291258` (`event_id`);
ALTER TABLE event ADD KEY `FKevent429659` (`terminal_id`);
ALTER TABLE event ADD CONSTRAINT `fk_event_host_resp` FOREIGN KEY (`host_response_id`) REFERENCES `inp_host_response` (`id`);
ALTER TABLE event ADD CONSTRAINT `fk_event_inp_event` FOREIGN KEY (`event_id`) REFERENCES `inp_event` (`id`);
ALTER TABLE event ADD CONSTRAINT `fk_event_term` FOREIGN KEY (`terminal_id`) REFERENCES `virtual_terminal` (`id`);
