import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.ThreadLocalRandom;

class UniPosTransaction {

    String transactionFile;
    String eventsFile;


    int trycount = 1;

    int[] paymentSystems = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};

    int[] terminalCodes = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
            41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60,
            61, 62, 63, 64, 65, 66, 67, 72, 73, 74, 75, 76, 77, 78};

    int[] cardInputTypeId = {1, 2, 3, 4};

    int[] responseId = {1, 2, 3, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
            , 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39
            , 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59
            , 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79
            , 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99
            , 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 112, 113, 114, 120, 131, 132
            , 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148
            , 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164
            , 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180
            , 181, 182, 183, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 370, 371
            , 372, 373, 374, 375, 376, 377, 378, 1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007
            , 1008, 1009, 1010, 1011, 1012, 1013, 1014, 1015, 1016, 1017, 1018, 1019, 1020, 1021
            , 1022, 1023, 1024, 1025, 1026, 1027, 1028, 1029, 1030, 1031, 1032, 1033, 1034, 1035
            , 1036, 1037, 1038, 1039, 1040, 1041, 1042, 1043, 1044, 1045, 1046, 1047, 1048, 1049
            , 1050, 1051, 1052, 1053, 1054, 1055, 1056, 1057, 1058};

    int[] eventId = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20
            , 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40
            , 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60
            , 61, 62, 63, 64, 65, 66, 67, 72, 73, 74, 75, 76, 77, 78};

    int currencyId = 1;
    int[] bin = {546938, 553691, 485078, 415481};
    String responseCode = "04";
    int amountDecimal = 39999;
    String creationDate = "\\N";

    static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    UniPosTransaction() {
    }

    int getRandomPaymentSystem() {
        return paymentSystems[ThreadLocalRandom.current().nextInt(paymentSystems.length)];
    }

    int getCardInputTypeId() {
        return cardInputTypeId[ThreadLocalRandom.current().nextInt(cardInputTypeId.length)];
    }

    int getResponseId() {
        return responseId[ThreadLocalRandom.current().nextInt(responseId.length)];
    }


    int getBin() {
        return bin[ThreadLocalRandom.current().nextInt(bin.length)];
    }

    int getEventId() {
        return eventId[ThreadLocalRandom.current().nextInt(eventId.length)];
    }

    UniPosEvent uniPosEvent = new UniPosEvent();

    BufferedWriter createTransactionFile() throws IOException {

        OutputStream outputStream = new FileOutputStream(transactionFile);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        BufferedWriter file = new BufferedWriter(outputStreamWriter);
        return file;
    }

    BufferedWriter createEventsFile() throws IOException{

        OutputStream outputStream = new FileOutputStream(eventsFile);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        BufferedWriter file = new BufferedWriter(outputStreamWriter);
        return file;
    }

    void generateToFile(int amountOfPos, long startDateTime, long endDateTime, int transactionFrequency,
                        int amountOfEventsPerTransaction, int idOfFirstVirtualTerminal,
                        long lastTransactionId, long lastEventId, String transactionFile, String eventsFile) {

        this.transactionFile = transactionFile;
        this.eventsFile = eventsFile;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        try {

            long frequencyInMillis = transactionFrequency * 60 * 1000;

            System.out.println("generating data from " + dateTimeFormat.format(startDateTime) + " to " + dateTimeFormat.format(endDateTime));

            try {
                int test = 1000 / (int)(frequencyInMillis / amountOfPos);

            } catch (Throwable ex) {
                System.out.println( "too many terminals and too little frequency time");
            }

            BufferedWriter transactions = createTransactionFile();
            BufferedWriter events = createEventsFile();

            long transactionId = lastTransactionId + 1;

            long rightNow = new GregorianCalendar().getTimeInMillis();


            while (startDateTime < endDateTime) {
                StringBuilder oneTransaction = new StringBuilder();
                String separator = ",";
                long currentTransactionDateSec = startDateTime;
                long latencyBetweenTerminals = (int)frequencyInMillis/amountOfPos;



                for (int i = 0; i < amountOfPos; i++) {
                    oneTransaction.delete(0, oneTransaction.length());

                    int currentTerminalId = idOfFirstVirtualTerminal + i;
                    currentTransactionDateSec += latencyBetweenTerminals;

                    String transaction_date = simpleDateFormat.format(currentTransactionDateSec);
                    creationDate = simpleDateFormat.format(rightNow);

                    oneTransaction.append(transactionId)
                            .append(separator)
                            .append(currentTransactionDateSec/1000)
                            .append(separator)
                            .append(ThreadLocalRandom.current().nextInt(5))
                            .append(separator)
                            .append(trycount)
                            .append(separator)
                            .append(getRandomPaymentSystem())
                            .append(separator)
                            .append(getCardInputTypeId())
                            .append(separator)
                            .append(getResponseId())
                            .append(separator)
                            .append(getEventId())
                            .append(separator)
                            .append(currentTerminalId)
                            .append(separator)
                            .append(currencyId)
                            .append(separator)
                            .append(getBin())
                            .append(separator)
                            .append(responseCode)
                            .append(separator)
                            .append(amountDecimal)
                            .append(separator)
                            .append(creationDate)
                            .append(separator)
                            .append(transaction_date)
                            .append("\n");

                    transactions.write(oneTransaction.toString());

                    uniPosEvent.generateEvents(events, amountOfEventsPerTransaction, currentTransactionDateSec, currentTerminalId, lastEventId);
                    transactionId++;
                    lastEventId = lastEventId + amountOfEventsPerTransaction;

                }
                startDateTime += frequencyInMillis;

            }

            transactions.close();
            events.close();
            System.out.println("Created " + transactionFile);
            System.out.println("Created " + eventsFile);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
