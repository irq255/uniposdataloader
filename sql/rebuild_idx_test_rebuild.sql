/*
ALTER TABLE TRANSACTION DROP CONSTRAINT "FK_IDXTBIULD_CARD_INPUT";
ALTER TABLE TRANSACTION DROP CONSTRAINT "FK_IDXTBIULD_CUR";
ALTER TABLE TRANSACTION DROP CONSTRAINT "FK_IDXTBIULD_EVENT";
ALTER TABLE TRANSACTION DROP CONSTRAINT "FK_IDXTBIULD_PAY_SYSTEM";
ALTER TABLE TRANSACTION DROP CONSTRAINT "";
ALTER TABLE TRANSACTION DROP CONSTRAINT "";
DROP INDEX "PK_TRANSACTION";
DROP INDEX "FKTRANSACTIO181655";
DROP INDEX "FKTRANSACTIO57240";
DROP INDEX "FKTRANSACTIO546647"
DROP INDEX "FKTRANSACTIO403039";
DROP INDEX "IDX$TR$DATE_TIME";
DROP INDEX "FKTRANSACTIO392658";

*/

DROP TABLE "MONITORING2"."INDEX_TEST_BUILDING" cascade constraints;

  CREATE TABLE "MONITORING2"."INDEX_TEST_BUILDING" 
   (  "ID" NUMBER(38,0), 
  "DATE_TIME" NUMBER(38,0), 
  "RUNTIME" NUMBER(10,0) DEFAULT 1, 
  "TRY_COUNT" NUMBER(10,0) DEFAULT 1, 
  "PAYMENT_SYSTEM_ID" NUMBER(38,0), 
  "CARD_INPUT_TYPE_ID" NUMBER(38,0), 
  "RESPONSE_ID" NUMBER(38,0), 
  "EVENT_ID" NUMBER(38,0), 
  "VIRTUAL_TERMINAL_ID" NUMBER(38,0), 
  "CURRENCY_ID" NUMBER(38,0), 
  "BIN" NUMBER(38,0), 
  "RESPONSE_CODE" VARCHAR2(20 BYTE), 
  "AMOUNT_DECIMAL" NUMBER(18,0), 
  "CREATION_DATE" DATE, 
  "TRANSACTION_DATE" DATE
   ) SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

  --------------------------------------------------------
--  DDL for Index FKTRANSACTIO181655
--------------------------------------------------------

  CREATE INDEX "MONITORING2"."FKINDEX_TEST_BUILDING181655" ON "MONITORING2"."INDEX_TEST_BUILDING" ("CURRENCY_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

  --------------------------------------------------------
--  DDL for Index FKTRANSACTIO57240
--------------------------------------------------------

  CREATE INDEX "MONITORING2"."FKINDEX_TEST_BUILDING57240" ON "MONITORING2"."INDEX_TEST_BUILDING" ("PAYMENT_SYSTEM_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

    CREATE UNIQUE INDEX "MONITORING2"."PK_INDEX_TEST_BUILDING" ON "MONITORING2"."INDEX_TEST_BUILDING" ("ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

  --------------------------------------------------------
--  DDL for Index FKTRANSACTIO546647
--------------------------------------------------------

  CREATE INDEX "MONITORING2"."FKINDEX_TEST_BUILDING546647" ON "MONITORING2"."INDEX_TEST_BUILDING" ("EVENT_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

  --------------------------------------------------------
--  DDL for Index FKTRANSACTIO403039
--------------------------------------------------------

  CREATE INDEX "MONITORING2"."FKIDXTBIULD03039" ON "MONITORING2"."INDEX_TEST_BUILDING" ("VIRTUAL_TERMINAL_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

  --------------------------------------------------------
--  DDL for Index IDX$TR$DATE_TIME
--------------------------------------------------------

  CREATE INDEX "MONITORING2"."IDX$IDXTBIULD$DATE_TIME" ON "MONITORING2"."INDEX_TEST_BUILDING" ("DATE_TIME") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;


  --------------------------------------------------------
--  DDL for Index FKTRANSACTIO392658
--------------------------------------------------------

  CREATE INDEX "MONITORING2"."FKIDXTBIULD392658" ON "MONITORING2"."INDEX_TEST_BUILDING" ("RESPONSE_ID") 
  PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM" ;

  --------------------------------------------------------
--  Constraints for Table TRANSACTION
--------------------------------------------------------

  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" ADD CONSTRAINT "PK_INDEX_TEST_BUILDING" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "SYSTEM"  ENABLE;
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" MODIFY ("CURRENCY_ID" NOT NULL ENABLE);
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" MODIFY ("VIRTUAL_TERMINAL_ID" NOT NULL ENABLE);
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" MODIFY ("EVENT_ID" NOT NULL ENABLE);
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" MODIFY ("PAYMENT_SYSTEM_ID" NOT NULL ENABLE);
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" MODIFY ("DATE_TIME" NOT NULL ENABLE);
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" MODIFY ("ID" NOT NULL ENABLE);


  --------------------------------------------------------
--  Ref Constraints for Table TRANSACTION
--------------------------------------------------------

  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" ADD CONSTRAINT "FK_IDXTBIULD_CARD_INPUT" FOREIGN KEY ("CARD_INPUT_TYPE_ID")
    REFERENCES "MONITORING2"."INP_CARD_INPUT_TYPE" ("ID") ENABLE;
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" ADD CONSTRAINT "FK_IDXTBIULD_CUR" FOREIGN KEY ("CURRENCY_ID")
    REFERENCES "MONITORING2"."CURRENCY" ("ID") ENABLE;
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" ADD CONSTRAINT "FK_IDXTBIULD_EVENT" FOREIGN KEY ("EVENT_ID")
    REFERENCES "MONITORING2"."INP_EVENT" ("ID") ENABLE;
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" ADD CONSTRAINT "FK_IDXTBIULD_PAY_SYSTEM" FOREIGN KEY ("PAYMENT_SYSTEM_ID")
    REFERENCES "MONITORING2"."INP_PAYMENT_SYSTEMS" ("ID") ENABLE;
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" ADD CONSTRAINT "FK_IDXTBIULD_RESPONSE" FOREIGN KEY ("RESPONSE_ID")
    REFERENCES "MONITORING2"."INP_HOST_RESPONSE" ("ID") ENABLE;
  ALTER TABLE "MONITORING2"."INDEX_TEST_BUILDING" ADD CONSTRAINT "FK_IDXTBIULD_VIRT_TERM" FOREIGN KEY ("VIRTUAL_TERMINAL_ID")
    REFERENCES "MONITORING2"."VIRTUAL_TERMINAL" ("ID") ENABLE;