load data local infile '/Users/konstantinlivenskiy/projects/UniPos/DataLoader/data/transactions.csv' into table transaction
FIELDS TERMINATED BY ','
#ENCLOSED BY '"'
LINES TERMINATED BY '\n'
	(id
		,date_time
		,runtime
		,try_count
		,payment_system_id
		,card_input_type_id
		,response_id
		,event_id
		,virtual_terminal_id
		,currency_id
		,bin
		,response_code
		,amount_decimal
		,creation_date
		,transaction_date );
