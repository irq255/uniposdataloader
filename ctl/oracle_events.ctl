load data
infile '/opt/test/oraloader/data/events.csv' into table event
append
FIELDS TERMINATED BY ','
	(id
	, date_time
	, event_id
	, terminal_id
	, value
	, host_response_id
	, response_code terminated by whitespace)