select table_schema "full_db_size", round(sum(data_length + index_length)/1024/1024) "size" from information_schema.tables
group by table_schema
having table_schema = 'monitoring';

select table_schema "database"
				,table_name
				,round(data_length / 1024 / 1024, 1) "data_size"
				,round(index_length/ 1024 / 1024, 1) "index_size"
				,round(sum(data_length + index_length)/ 1024 / 1024, 1) "full_size" 
	from information_schema.tables where table_schema = 'monitoring'
	group by table_name
	order by full_size desc;