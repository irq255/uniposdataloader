import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

public class App {

    private static Config config = new Config();
    private static int amountOfPos = Integer.parseInt(config.getProperty("amountOfPos"));
    private static int amountOfEventsPerTransaction = Integer.parseInt(config.getProperty("amountOfEventsPerTransaction"));

    private static int transactionFrequency = Integer.parseInt(config.getProperty("transactionFrequency"));
    private static int transactionPoolTime = Integer.parseInt(config.getProperty("transactionPoolTime"));
    private static String transactionFile = "." + config.getProperty("transactionsFile");
    private static String eventsFile = "." + config.getProperty("eventsFile");
    private static String generateStartDay = config.getProperty("generateStartDay") + " 00:00:00";
    private static String generateEndDay = config.getProperty("generateEndDay") + " 00:00:00";
    private static String loadTransactionsSqlScript = config.getProperty("loadTransactionsSqlScript");
    private static String loadEventsSqlScript = config.getProperty("loadEventsSqlScript");
    private static String loadIndexTestBuildingScript = config.getProperty("loadIndexTestBuildingScript");
    private static int IdOfFirstVirtualTerminal = new OracleRequester().getIdOfFirstVirtualTerminal();
    private static boolean LOAD_TRANSACTIONS_TRIGGER = "1".equals(config.getProperty("trigger_loadTransactions"));
    private static boolean LOAD_EVENTS_TRIGGER = "1".equals(config.getProperty("trigger_loadEvents"));
    private static boolean LOAD_INDEX_TEST_BUILDING = "1".equals(config.getProperty("trigger_indexTestBuilding"));


    public static void main(String[] args) throws ParseException {


        SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


        long hours = transactionPoolTime * 3600 * 1000;
        long currentTransactionStartDay = dateTimeFormat.parse(generateStartDay).getTime();

        long BEGIN = System.nanoTime();

        while (currentTransactionStartDay < dateTimeFormat.parse(generateEndDay).getTime()) {
            long currentTransactionEndDay = currentTransactionStartDay + hours;
            OracleRequester askDatabase = new OracleRequester();
            long lastTransactionId = askDatabase.getLastTransactionId();
            long lastEventId = askDatabase.getLastEventId();
            long lastIndexTestRebuild = askDatabase.getLastIndexTestRebuildId();


            long LAP = System.nanoTime();
            System.out.println("\n" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new GregorianCalendar().getTimeInMillis()) + ": Start preparing csv");

            new UniPosTransaction().generateToFile(amountOfPos, currentTransactionStartDay, currentTransactionEndDay, transactionFrequency
                    , amountOfEventsPerTransaction, IdOfFirstVirtualTerminal
                    , lastTransactionId, lastEventId, transactionFile, eventsFile);


            OracleLoader loadTransactions = new OracleLoader(loadTransactionsSqlScript);

            OracleLoader loadIndexTestBuilding = new OracleLoader(loadIndexTestBuildingScript);

            OracleLoader loadEvents = new OracleLoader(loadEventsSqlScript);




            if (LOAD_TRANSACTIONS_TRIGGER) {
                System.out.println("transactions trigger " + LOAD_TRANSACTIONS_TRIGGER + " . Start load");
                loadTransactions.load();

            } else {
                System.out.println("transactions trigger " + LOAD_TRANSACTIONS_TRIGGER);
            }

            if (LOAD_EVENTS_TRIGGER) {
                System.out.println("events trigger " + LOAD_EVENTS_TRIGGER + " . Start load");
                loadEvents.load();

            } else {
                System.out.println("events trigger " + LOAD_EVENTS_TRIGGER);
            }

            if (LOAD_INDEX_TEST_BUILDING) {
                System.out.println("index test building trigger " + LOAD_INDEX_TEST_BUILDING + " . Start load");
                loadIndexTestBuilding.load();
            }


            timer(LAP);
            currentTransactionStartDay += hours;

        }
        System.out.println("---\nFinishing");
        timer(BEGIN);

    }

    static void timer(long time) {
        System.out.println("Total : " + (System.nanoTime() - time) / 1000000000 + " seconds\n");
    }
}
